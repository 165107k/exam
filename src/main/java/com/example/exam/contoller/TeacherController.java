package com.example.exam.contoller;


import com.example.exam.model.Student;
import com.example.exam.model.Teacher;
import com.example.exam.service.TeacherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/teacher")
@Tag(name = "teacher", description = "АПИ для учителя")
public class TeacherController {


    @Autowired
    private TeacherService teacherService;

    @GetMapping("/{id}")
    @Operation(summary = "Получение учителя по идентификатору")
    public Teacher getTeacherById(@PathVariable Long id){
        Teacher teacher = teacherService.findById(id);
        return teacher;
    }

    @GetMapping("/all")
    @Operation(summary = "Получение всех учителей")
    public List<Teacher> getTeacherAll(){
        List<Teacher> teachers = teacherService.findAll();
        return teachers;
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление учителя по идентификатору")
    public void deliteTeacherById(@PathVariable Long id){
        teacherService.deleteById(id);
    }

    @PutMapping("")
    @Operation(summary = "Редактирование учителя")
    public Teacher changeTeacher(@RequestBody Teacher teacher){
        Teacher teacher2 = teacherService.saveTeacher(teacher);
        return teacher2;
    }
    @GetMapping("/teachersasstudents/{id}")
    @Operation(summary = "Студенты у преподавателя")
    public List<Student> getTeachersAsStudents(@PathVariable Long id){
        List<Student> studentList = teacherService.findStudentThisTeacher(id);


        for (int i=0; i < studentList.size(); i++){
            studentList.get(i).setTeacherList(null);
        }
        return studentList;
    }

    @PostMapping()
    @Operation(summary = "Создать учителя")
    public ResponseEntity<Teacher> saveTeacher(@RequestBody Teacher teacher){
        if(teacher == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        teacherService.saveTeacher(teacher);

        for (int i=0; i < teacher.getStudent().size(); i++){
            teacher.getStudent().get(i).setTeacherList(null);
        }
        return new ResponseEntity<>(teacher, HttpStatus.CREATED);
    }

}
