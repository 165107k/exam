package com.example.exam.contoller;

import com.example.exam.model.Student;
import com.example.exam.model.Teacher;
import com.example.exam.service.StudentService;
import com.example.exam.service.TeacherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/student")
@Tag(name = "student", description = "АПИ для студента")
public class StudentController {


    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    @GetMapping("/{id}")
    @Operation(summary = "Получение студента по идентификатору")
    public Student getStudentById(@PathVariable Long id){
        Student student = studentService.findById(id);
        return student;
    }

    @GetMapping("/all")
    @Operation(summary = "Получение всех студентов")
    public List<Student> getTeacherAll(){
        List<Student> students = studentService.findAll();
        return students;
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление студента по идентификатору")
    public void deliteTeacherById(@PathVariable Long id){
        studentService.deleteById(id);
    }

    @PutMapping("/assign/student/{ids}/teacher/{idt}")
    public Student addTeatherThisStudent(@PathVariable Long ids, @PathVariable Long idt){
        Student student = studentService.findById(ids);
        Teacher teacher = teacherService.findById(idt);

        studentService.addTeatherThisStudent(student, teacher);

        for (int i=0; i < student.getTeacherList().size(); i++){
            student.getTeacherList().get(i).setStudent(null);
        }
        return student;
    }


    @PutMapping("/removestudentthisteacher/student/{ids}/teacher/{idt}")
    @Operation(summary = "Удаление студента у преподавателя")
    public Student getDeliteStudentThisTeacher(@PathVariable Long ids, @PathVariable Long idt){
        Student student = studentService.findById(ids);
        Teacher teacher = teacherService.findById(idt);

        studentService.deliteStudentThisTeacher(ids, idt);

        for (int i=0; i < student.getTeacherList().size(); i++){
            student.getTeacherList().get(i).setStudent(null);
        }
        return student;
    }


    @GetMapping("/studentsasteachers/{id}")
    @Operation(summary = "Преподаватели у студента")
    public ResponseEntity<List<Teacher>> getStudentsAsTeachers(@PathVariable Long id){
        List<Teacher> teacherList = studentService.findTeaherThisStudent(id);
        if(teacherList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        for (int i=0; i < teacherList.size(); i++){
            teacherList.get(i).setStudent(null);
        }
        return new ResponseEntity<>(teacherList, HttpStatus.OK);
    }

    @PostMapping()
    @Operation(summary = "Добавление студента")
    public ResponseEntity<Student> saveStudent(@RequestBody Student student){
        if(student == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        studentService.saveStudent(student);
        for (int i=0; i < student.getTeacherList().size(); i++){
            student.getTeacherList().get(i).setStudent(null);
        }
        return new ResponseEntity<>(student, HttpStatus.CREATED);
    }

}