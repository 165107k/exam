package com.example.exam.service;

import com.example.exam.dao.RoleRepository;
import com.example.exam.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Role findById(Long id){
        return roleRepository.getOne(id);
    }

    public List<Role> findAll(){
        return roleRepository.findAll();
    }

    public Role saveRole(Role role){
        return roleRepository.save(role);
    }

    public void deleteById(Long id){
        roleRepository.deleteById(id);
    }
}
