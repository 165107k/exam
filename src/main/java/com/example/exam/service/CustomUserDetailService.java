package com.example.exam.service;

import com.example.exam.model.Account;
import com.example.exam.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailService implements UserDetailsService {


    @Autowired
    private AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Account account = accountService.findByEmail(email);

        if(account == null){
            throw new UsernameNotFoundException("Unknown user:" + email);
        }

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        List<Role> roles = account.getRoles();
        for (int i = 0; i < roles.size(); i++){
            grantedAuthorities.add(new SimpleGrantedAuthority(roles.get(i).getName()));
        }

        UserDetails userDetails = User.builder()
                .username(account.getEmail())
                .password(account.getPassword())
                .authorities(grantedAuthorities)
                .build();

        return userDetails;
    }

}
