package com.example.exam.service;


import com.example.exam.dao.StudentRepository;
import com.example.exam.dao.TeacherRepository;
import com.example.exam.model.Student;
import com.example.exam.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    public Student findById(Long id){
        return studentRepository.getOne(id);
    }

    public List<Student> findAll(){
        return studentRepository.findAll();
    }

    public Student saveStudent(Student student){
        return studentRepository.save(student);
    }

    public void deleteById(Long id){
        studentRepository.deleteById(id);
    }

    public Student addTeatherThisStudent(Student student, Teacher teacher){

        List<Teacher> studentList = new LinkedList<>();
        studentList.add(teacher);
        student.setTeacherList(studentList);
        studentRepository.save(student);

        return student;
    }

    public List<Teacher> deliteStudentThisTeacher(Long ids, Long idt){
        Student student = studentRepository.getOne(ids);
        List<Teacher> teacherList = new LinkedList<>();

        for(int i = 0; i < student.getTeacherList().size(); i++){
            teacherList.add(studentRepository.getOne(ids).getTeacherList().get(i));
        }
        for(int i = 0; i < student.getTeacherList().size(); i++){
            if(teacherList.get(i).getTeacher_id() == idt){
                teacherList.remove(i);
            }
        }

        student.setTeacherList(teacherList);
        studentRepository.save(student);
        return teacherList;
    }

    public List<Teacher> findTeaherThisStudent(Long id){
        Student student = studentRepository.getOne(id);
        List<Teacher> teacherList = new LinkedList<>();

        for(int i = 0; i < student.getTeacherList().size(); i++){
            teacherList.add(student.getTeacherList().get(i));
        }
        return teacherList;
    }


}
