package com.example.exam.service;


import com.example.exam.dao.StudentRepository;
import com.example.exam.dao.TeacherRepository;
import com.example.exam.model.Student;
import com.example.exam.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;

    public Teacher findById(Long id){
        return teacherRepository.getOne(id);
    }

    public List<Teacher> findAll(){
        return teacherRepository.findAll();
    }

    public Teacher saveTeacher(Teacher teacher){
        return teacherRepository.save(teacher);
    }

    public void deleteById(Long id){
        teacherRepository.deleteById(id);
    }

    public List<Student> findStudentThisTeacher(Long id){
        Teacher teacher = teacherRepository.getOne(id);
        List<Student> studentList = new LinkedList<>();

        for(int i = 0; i < teacher.getStudent().size(); i++){
            studentList.add(teacher.getStudent().get(i));
        }
        return studentList;
    }

}
