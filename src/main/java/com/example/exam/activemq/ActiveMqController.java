package com.example.exam.activemq;


import com.example.exam.model.Student;
import com.example.exam.model.Teacher;
import com.example.exam.service.StudentService;
import com.example.exam.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/active")
public class ActiveMqController {

    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private StudentService studentService;


    //По данному алгоритму можно сделать все сущности. P.S "Получение студента по идентификатору" я ловлю рекурсию
    //это можно исправить циклом for или с помощью @JsonIdentityInfo (реализовано в дз), забыл протестить
    //на экзамене, а сейчас боюсь не попасть на проверку работ изза этого не испраляю. Где то у меня прописан
    //for, а где то я не его потерял. Пример как можно было бы сделать с for-ом ниже.
    @GetMapping("/getMeLocalStudent")
    private Student getStudent (){
        Student student = new Student(2l,"gg","fff", "fff", "ddd", 3);

        if(student.getTeacherList() != null){
            for (int i=0; i < student.getTeacherList().size(); i++){
                student.getTeacherList().get(i).setStudent(null);
            }
        }

        jmsTemplate.convertAndSend("student", student);
        return student;
    }

    @GetMapping("/send/{massage}")
    private String getMessege (@PathVariable String massage){
        jmsTemplate.convertAndSend("massage", massage);
        return massage;
    }

}
