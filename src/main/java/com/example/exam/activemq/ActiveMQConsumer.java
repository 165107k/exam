package com.example.exam.activemq;


import com.example.exam.model.Student;
import com.example.exam.model.Teacher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ActiveMQConsumer {

    @JmsListener(destination = "student")
    public void processMessage(Student student){
        log.info(student.getFirst_name() + " " + student.getLast_name() + " " + student.getMiddle_name());
    }

    @JmsListener(destination = "massage")
    public void processMessage(String massage){
        log.info(massage);
    }
}
