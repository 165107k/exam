package com.example.exam.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "role_id")

@Entity
public class Role{

    @Id
    @GeneratedValue
    private Long role_id;

    private String name;

    @ManyToMany(mappedBy = "roles",
            fetch = FetchType.EAGER)
    private List<Account> accounts;

    public Role() {
    }

    public Role(Long role_id, String name, List<Account> accounts) {
        this.role_id = role_id;
        this.name = name;
        this.accounts = accounts;
    }

    public Long getRole_id() {
        return role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

}

